import 'package:flutter/material.dart';

void main() => runApp(HelloFlutterApp());

class HelloFlutterApp extends StatefulWidget {
  @override
  _HelloFlutterAppState createState() => _HelloFlutterAppState();
}

String englishGreeting = "Hello Flutter";
String spanishGreeting = "Hola Flutter";
String japanGreeting = "Kon'nichiwa Flutter";
String koreanGreeting = "an nyeong ha seyo Flutter";

class _HelloFlutterAppState extends State<HelloFlutterApp> {
  String displayText = englishGreeting;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: const Text("Hello Flutter"),
          leading: Icon(Icons.home),
          actions: <Widget>[
            IconButton(
                onPressed: () {
                  setState(() {
                    displayText = displayText == englishGreeting
                        ? japanGreeting
                        : englishGreeting;
                  });
                },
                icon: const Icon(Icons.g_translate_sharp)),
            IconButton(
                onPressed: () {
                  setState(() {
                    displayText = displayText == englishGreeting
                        ? koreanGreeting
                        : englishGreeting;
                  });
                },
                icon: const Icon(Icons.g_translate_sharp)),
            IconButton(
                onPressed: () {
                  setState(() {
                    displayText = displayText == englishGreeting
                        ? spanishGreeting
                        : englishGreeting;
                  });
                },
                icon: const Icon(Icons.g_translate_sharp))
          ],
        ),
        body: Center(
          child: Text(displayText, style: TextStyle(fontSize: 36)),
        ),
      ),
    );
  }
}
// class HelloFlutterApp extends StatelessWidget{
//   @override

//
// }
